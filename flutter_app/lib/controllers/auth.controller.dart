import 'package:dio/dio.dart';
import 'package:flutter_app/config/server.config.dart';
import 'package:flutter_app/models/login.dto.dart';

class AuthController {
  static String endpoint = "users/auth/";

  static Future<Response> login(LoginDto loginDto) {
    String url = "${ServerConfig.apiUrl}/$endpoint/sign_in";

    Dio dio = Dio();
    return dio.post(url,
        data: loginDto.toJson(),
        options: Options(
            contentType: Headers.jsonContentType,
            responseType: ResponseType.json,
            validateStatus: (_) => true));
  }
}
