import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_app/config/server.config.dart';

class EnterprisesController {
  static String endpoint = "enterprises";

  static Future<Response> getEnterprise(int id) {
    String url = "${ServerConfig.apiUrl}/$endpoint/$id";

    Dio dio = Dio();
    return dio.get(
      url,
      options: Options(
          headers: {
            'Content-Type': 'application/json',
            'access-token': ServerConfig.accessToken,
            'client': ServerConfig.client,
            'uid': ServerConfig.uid
          },
          contentType: Headers.jsonContentType,
          responseType: ResponseType.json,
          validateStatus: (_) => true),
    );
  }

  static Future<Response> getEnterprises({String name}) {
    String url =
        "${ServerConfig.apiUrl}/$endpoint${name != null ? '?enterprise_name=\"$name\"' : ''}";

    Dio dio = Dio();
    return dio.get(url,
        options: Options(
            headers: {
              'Content-Type': 'application/json',
              'access-token': ServerConfig.accessToken,
              'client': ServerConfig.client,
              'uid': ServerConfig.uid
            },
            contentType: Headers.jsonContentType,
            responseType: ResponseType.json,
            validateStatus: (_) => true));
  }
}
