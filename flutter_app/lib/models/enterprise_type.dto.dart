class EnterpriseTypeDto {
  int id;
  String name;

  EnterpriseTypeDto.fromJson(Map<String, dynamic> json) {
    id = int.tryParse(json["id"].toString()) ?? 0;
    name = json["enterprise_type_name"];
  }
}
