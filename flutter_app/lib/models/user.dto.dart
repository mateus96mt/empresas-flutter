import 'package:flutter_app/models/enterprise.dto.dart';
import 'package:flutter_app/models/investor.dto.dart';

class UserDto {
  InvestorDto investorDto;
  EnterpriseDto enterpriseDto;

  UserDto.fromJson(Map<String, dynamic> json) {
    investorDto = json["investor"] != null
        ? InvestorDto.fromJson(json["investor"])
        : null;
    enterpriseDto = json["enterprise"] != null
        ? EnterpriseDto.fromJson(json["enterprise"])
        : null;
  }
}
