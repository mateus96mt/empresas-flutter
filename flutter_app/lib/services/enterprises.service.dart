import 'package:flutter_app/controllers/enterprise.controller.dart';
import 'package:flutter_app/models/enterprise.dto.dart';

class EnterprisesService {
  static Future<List<EnterpriseDto>> getEnterprises({String name}) async {
    List<EnterpriseDto> enterpriseList = List();

    await EnterprisesController.getEnterprises(name: name).then((response) {

      List<dynamic> list;
      list = response.data["enterprises"];

      for (Map<String, dynamic> json in list) {
        enterpriseList.add(EnterpriseDto.fromJson(json));
      }
    });
    return enterpriseList;
  }
}
