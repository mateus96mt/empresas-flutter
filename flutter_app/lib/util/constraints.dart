const double fontSizeHuge = 24;
const double fontSizeBig = 18;
const double fontSizeMedium = 15;
const double fontSizeSmall = 12;
