import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/config/server.config.dart';
import 'package:flutter_app/config/size.config.dart';
import 'package:flutter_app/controllers/auth.controller.dart';
import 'package:flutter_app/models/login.dto.dart';
import 'package:flutter_app/models/user.dto.dart';
import 'package:flutter_app/util/constraints.dart';
import 'package:flutter_app/views/home.view.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

UserDto loggedUserDto;

class LoginScreen extends StatefulWidget {
  LoginScreen() : super();

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  bool isLoading = false;
  bool areCredentialsWrong = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: (MediaQuery.of(context).viewInsets.bottom == 0)
                  ? SizeConfig.screenHeight / 3.0
                  : SizeConfig.screenHeight / 6.0,
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100),
                        bottomRight: Radius.circular(100)),
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        Colors.purple,
                        Colors.deepOrange,
                      ],
                    ),
                  ),
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage('assets/images/icon_ioasys.png'),
                        color: Colors.white,
                        height: 50,
                        width: 50,
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      MediaQuery.of(context).viewInsets.bottom == 0
                          ? Text(
                              "Seja bem vindo ao empresas!",
                              style: TextStyle(
                                  color: Colors.white, fontSize: fontSizeBig),
                            )
                          : Container()
                    ],
                  )),
                ),
              ),
            ),
            Container(
              height: 2.0 * SizeConfig.screenHeight / 3.0,
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Email"),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    customTextField(emailTextEditingController),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    Text("Senha"),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                    customTextField(passwordTextEditingController,
                        obscureText: true),
                    areCredentialsWrong
                        ? Row(
                            children: [
                              Expanded(child: Container()),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                  "Credenciais incorretas",
                                  style: TextStyle(color: Colors.red),
                                ),
                              ),
                            ],
                          )
                        : Container(),
                    Padding(padding: EdgeInsets.only(bottom: 50)),
                    Container(
                      width: SizeConfig.screenWidth,
                      height: SizeConfig.screenWidth / 8.0,
                      child: Padding(
                        padding: EdgeInsets.only(right: 10, left: 10),
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                if (states.contains(MaterialState.pressed))
                                  return isLoading
                                      ? Colors.grey.withOpacity(0.6)
                                      : Colors.red;
                                return isLoading
                                    ? Colors.grey.withOpacity(0.6)
                                    : Colors
                                        .pinkAccent; // Use the component's default.
                              },
                            ),
                          ),
                          child: isLoading
                              ? SpinKitDualRing(
                                  size: 40,
                                  color: Colors.white,
                                )
                              : Text(
                                  "ENTRAR",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fontSizeBig),
                                ),
                          onPressed: isLoading ? null : makeLogin,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void makeLogin() {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
    }
    LoginDto loginDto = LoginDto(
        emailTextEditingController.text, passwordTextEditingController.text);
    AuthController.login(loginDto).then((response) {
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }

      switch (response.statusCode) {
        case HttpStatus.ok:
          List<String> headers = response.headers.toString().split('\n');
          ServerConfig.accessToken = headers[8].split(' ')[1];
          ServerConfig.uid = headers[14].split(' ')[1];
          ServerConfig.client = headers[17].split(' ')[1];
          loggedUserDto = UserDto.fromJson(response.data);
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => HomeScreen()),
              (Route<dynamic> route) => false);
          break;
        default:
          if (mounted) {
            setState(() {
              areCredentialsWrong = true;
            });
          }
          break;
      }
    });
  }

  Widget customTextField(TextEditingController textEditingController,
      {bool obscureText = false}) {
    return Container(
      padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
        border: Border.all(
            color: areCredentialsWrong
                ? Colors.red
                : Colors.grey.withOpacity(0.2)),
      ),
      child: TextField(
        obscureText: obscureText,
        onTap: () {
          if (mounted) {
            setState(() {
              areCredentialsWrong = false;
            });
          }
        },
        decoration: InputDecoration(
          suffixIcon: areCredentialsWrong
              ? IconButton(
                  onPressed: textEditingController.clear,
                  icon: Icon(
                    Icons.clear,
                    color: Colors.red,
                  ),
                )
              : null,
          border: InputBorder.none,
        ),
        controller: textEditingController,
      ),
    );
  }
}
