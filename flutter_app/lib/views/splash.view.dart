import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/views/login.view.dart';

class SplashScreen extends StatefulWidget {
  final bool wasCalledFromMain;

  SplashScreen({this.wasCalledFromMain}) : super();

  @override
  _SplashScreenState createState() => _SplashScreenState(wasCalledFromMain);
}

class _SplashScreenState extends State<SplashScreen> {
  bool wasCalledFromMain;
  int delaySeconds = 4;

  _SplashScreenState(this.wasCalledFromMain);

  @override
  void initState() {
    wasCalledFromMain = wasCalledFromMain ?? false;
    if (wasCalledFromMain) {
      Timer(Duration(seconds: delaySeconds), () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LoginScreen()),
            (Route<dynamic> route) => false);
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Colors.purple,
              Colors.deepOrange,
            ],
          )),
          child: Center(
            child: Image(
              image: AssetImage('assets/images/logo_ioasys.png'),
              color: Colors.white,
              height: 200,
              width: 200,
            ),
          ),
        ),
      ),
    );
  }
}
